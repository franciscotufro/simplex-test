#include "Simplex/Test/Runner.h"
#include <iostream>

namespace Simplex
{
  namespace Test
  {
    Runner::Runner (int argc, char **argv)
    {
      #ifdef DEBUG
        std::cout << "Initializing Simplex::Test::Runner...";
      #endif
      
      testing::InitGoogleTest(&argc, argv);

      #ifdef DEBUG
        std::cout << " DONE.\n";
      #endif
      
    }
    
    int Runner::Start ()
    {
      return RUN_ALL_TESTS();
    }
  }
}

int main ( int argc, char **argv )
{
  Simplex::Test::Runner testRunner = Simplex::Test::Runner ( argc, argv );
  return testRunner.Start ();
}

